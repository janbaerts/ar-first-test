using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class EditorRemote : MonoBehaviour
{

    [SerializeField] private GameObject gaugePrefab;

    private Gauge gauge;

    private void Awake()
    {
        Assert.IsNotNull(gaugePrefab);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            Debug.Log("JB: Instantiating a gauge.");
            gauge = Instantiate(gaugePrefab).GetComponent<Gauge>();

            gauge.Initialize(25f, 100f, "m", "Afstand",
                new ValueAssessment[] { ValueAssessment.Low, ValueAssessment.Normal, ValueAssessment.High },
                new float[] { 25f, 35, 80, 100 });

            //gauge.Initialize(100, 25, "m", "Afstand",
            //    new ValueAssessment[] { ValueAssessment.Low, ValueAssessment.Normal, ValueAssessment.High },
            //    new float[] { 100, 80, 35, 25 });

            gauge.GaugeValue = 33;
        }

        if (gauge != null && gauge.GaugeValue < 100)
        {
            gauge.GaugeValue += 3f * Time.deltaTime;
        }
    }

}
