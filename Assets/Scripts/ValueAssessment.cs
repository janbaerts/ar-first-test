﻿using System;

public enum ValueAssessment
{
    Low = 0,
    Normal = 1,
    High = 2,
    Danger = 3,
    NotDefined = 88
}
