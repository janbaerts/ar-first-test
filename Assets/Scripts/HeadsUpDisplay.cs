using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class HeadsUpDisplay : MonoBehaviour
{

    [SerializeField] private TMP_Text statusText;
    public Button LocateMarkerButton;

    private static HeadsUpDisplay instance;
    public static HeadsUpDisplay Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<HeadsUpDisplay>();

            if (instance == null)
                Debug.LogError("JB: No HUD found in scene...");

            return instance;
        }
    }

    private void Awake()
    {
        Assert.IsNotNull(statusText);
        Log("HUD Status present...");

        Assert.IsNotNull(LocateMarkerButton);
        Log("Locate Marker Button present...");
    }

    public void Log(string message)
    {
        //statusText.text = DateTime.Now.ToLongTimeString() + " " + message + "\n" + statusText.text;
    }

    public void Show(string name)
    {
        statusText.text = name;
    }

}
