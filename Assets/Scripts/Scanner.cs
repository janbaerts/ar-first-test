using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.XR.ARSubsystems;

public class Scanner : MonoBehaviour
{
    [SerializeField] private XRReferenceObjectLibrary referenceLibrary;
    private XRObjectTrackingSubsystem trackingSubsystem;

    // Start is called before the first frame update
    void Start()
    {
        Assert.IsNotNull(referenceLibrary, "JB: Reference library not found...");

        var descriptors = new List<XRObjectTrackingSubsystemDescriptor>();
        SubsystemManager.GetSubsystemDescriptors(descriptors);

        Debug.Log($"JB: {descriptors.Count} descriptors found...");
        foreach (var descriptor in descriptors)
        {
            Debug.Log("JB: Descriptor found: " + descriptor.ToString());
            if (descriptor.GetType() == typeof(XRObjectTrackingSubsystemDescriptor))
                trackingSubsystem = descriptor.Create();
        }

        Assert.IsNotNull(trackingSubsystem, "JB: Object tracking subsystem not found...");

        trackingSubsystem.library = referenceLibrary;

        Debug.Log("JB: Starting object tracker...");
        trackingSubsystem.Start();
        InvokeRepeating("CheckForObjects", 1f, 1f);
    }

    private void CheckForObjects()
    {
        Debug.Log("JB: Checking changes in object tracking subsystem...");
        var scanResult = trackingSubsystem.GetChanges(Unity.Collections.Allocator.TempJob);
        if (scanResult.added.Length > 0)
            foreach (var result in scanResult.added)
                Debug.Log($"JB: {result.referenceObjectGuid.ToString()} found with pose {result.pose}.");
    }
}
