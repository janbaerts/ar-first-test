using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class Gauge : MonoBehaviour
{

    [SerializeField] private Image coloredDisplayImage;
    [SerializeField] private TMP_Text valueNameText;
    [SerializeField] private TMP_Text valueText;

    [SerializeField] private Color lowColor;
    [SerializeField] private Color normalColor;
    [SerializeField] private Color highColor;
    [SerializeField] private Color dangerColor;

    private Camera aRCamera;

    private Color[] assessmentColors;

    [SerializeField] private Notch[] notches;

    private Vector3 desiredPosition;
    public Vector3 DesiredPosition
    {
        get => desiredPosition;
        set
        {
            if (value != null)
            {
                desiredPosition = value;
                gameObject.transform.localPosition = desiredPosition;
            }
        }
    }

    private Vector3 desiredRotationEulers;
    //public Vector3 DesiredRotation { get; set; }
    public Vector3 DesiredRotation
    {
        get => desiredRotationEulers;
        set
        {
            desiredRotationEulers = value;
            if (value != null)
                gameObject.transform.eulerAngles = value;
        }
    }

    public bool HasAscendingValues { get; private set; }

    private ValueRange[] valueRanges;
    private float bottomValue;
    private float topValue;
    private string unit;
    public delegate void ShowInformation();
    public ShowInformation Show { get; set; }

    public bool HasBeenInitialized { get; private set; } = false;

    /// <summary>
    /// Range of the gauge as float between 0 and 1.
    /// </summary>
    private float gaugeRange = 0.75f;
    private float gaugeRangeRadial;

    private float gaugeValue;
    public float GaugeValue
    {
        get => gaugeValue;
        set
        {
            gaugeValue = value;
            SetGaugeValue(value);
            SetGaugeColor();
        }
    }

    public string ValueName
    {
        set
        {
            if (!string.IsNullOrEmpty(value))
                valueNameText.text = value;
            else
                valueNameText.text = "Waarde";
        }
    }

    private void Awake()
    {
        valueNameText.text = "Waarde";
        gaugeRangeRadial = 360 * gaugeRange * -1;
        assessmentColors = new Color[] { lowColor, normalColor, highColor, dangerColor };
    }

    // Start is called before the first frame update
    void Start()
    {
        Assert.IsNotNull(coloredDisplayImage);
        Assert.IsNotNull(valueNameText);
        Assert.IsNotNull(valueText);
        Assert.IsNotNull(notches);

        aRCamera = FindObjectOfType<Camera>();
        Assert.IsNotNull(aRCamera, "JB: No camera found in scene...");

        Debug.Log($"JB: Gauge {valueNameText.text} OK.");
    }

    private void Update()
    {
        //gameObject.transform.LookAt(aRCamera.transform, Vector3.down);
        Show();
    }

    private void SetGaugeValue(float gaugeValue)
    {
        coloredDisplayImage.fillAmount = GetValueFraction(gaugeValue);

        StringBuilder sb = new StringBuilder();

        if (unit == "m")
            sb.Append(gaugeValue.ToString("F2"));

        if (unit == "%")
            sb.Append(gaugeValue.ToString());

        sb.Append(" ");
        sb.Append(unit);
        valueText.text = sb.ToString();
    }

    private void SetGaugeColor()
    {
        var level = ValueRange.Assess(GaugeValue, valueRanges);

        if (level == ValueAssessment.NotDefined)
            return;

        coloredDisplayImage.color = assessmentColors[(int)level];
    }

    public void Initialize(float minimumValue, float maximumValue, string unit, string gaugeName,
        ValueAssessment[] levels, float[] thresholdValues)
    {

        if (thresholdValues.Length - levels.Length != 1)
        {
            Debug.LogError("JB: Level and threshold value mismatch.");
            return;
        }
        if (thresholdValues.Length <= 1)
        {
            Debug.LogError("JB: Must have at least 2 thresholdvalues.");
            return;
        }
        thresholdValues = Utils.SortThresholds(thresholdValues);
        HasAscendingValues = false;
        if (thresholdValues[0] < thresholdValues[thresholdValues.Length - 1])
            HasAscendingValues = true;

        valueNameText.text = gaugeName;

        this.bottomValue = thresholdValues[0];
        this.topValue = thresholdValues[thresholdValues.Length - 1];
        this.unit = unit;

        var allValues = thresholdValues;

        valueRanges = new ValueRange[levels.Length];

        for (int i = 0; i < levels.Length; i++)
            valueRanges[i] = new ValueRange(allValues[i], allValues[i + 1], levels[i]);

        notches[0].Configure(true, GetValueRotation(bottomValue), bottomValue + " " + unit);
        notches[1].Configure(true, GetValueRotation(topValue), topValue + " " + unit);

        for (int i = 2; i < allValues.Length; i++)
            notches[i].Configure(false, GetValueRotation(allValues[i - 1]), allValues[i - 1] + " " + unit);

        HasBeenInitialized = true;
    }

    private float GetValueFraction(float value) => GetValueInProportionToRange(value) * gaugeRange;
    private float GetValueRotation(float value) => GetValueInProportionToRange(value) * gaugeRangeRadial;

    private float GetValueInProportionToRange(float value)
    {
        if (HasAscendingValues)
        {
            value = Mathf.Clamp(value, bottomValue, topValue);
            value = value - bottomValue;
        }

        if (!HasAscendingValues)
        {
            value = Mathf.Clamp(value, topValue, bottomValue);
            value = bottomValue - value;
        }

        float range = Mathf.Abs(topValue - bottomValue);
        float result = value / range;
        return result;
    }
}
