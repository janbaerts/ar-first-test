﻿using System;
using System.Collections.Generic;

public static class Utils
{

    public static float[] SortThresholds(float[] thresholds)
    {
        if (thresholds == null)
            return null;

        if (thresholds.Length <= 1)
            return thresholds;

        List<float> thresholdList = new List<float>();
        foreach (float f in thresholds)
            thresholdList.Add(f);

        bool isAscending = false;
        if (thresholds[0] < thresholds[thresholds.Length - 1])
            isAscending = true;

        thresholdList.Sort();
        if (!isAscending)
            thresholdList.Reverse();

        return thresholdList.ToArray();
    }

}