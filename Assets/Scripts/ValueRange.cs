﻿using System;

public struct ValueRange
{

    /// <summary>
    /// This value is included.
    /// </summary>
    public float MinValue { get; set; }

    /// <summary>
    /// This value is excluded.
    /// </summary>
    public float MaxValue { get; set; }

    public ValueAssessment Assessment { get; set; }

    public ValueRange(float minValue, float maxValue, ValueAssessment assessment)
    {
        if (minValue < maxValue)
        {
            MinValue = minValue;
            MaxValue = maxValue;
        }
        else
        {
            MaxValue = minValue;
            MinValue = maxValue;
        }
        Assessment = assessment;
    }

    /// <summary>
    /// Minimum value included, maximum value excluded.
    /// </summary>
    /// <param name="valueToCheck"></param>
    /// <returns></returns>
    public bool Contains(float valueToCheck) => valueToCheck >= MinValue && valueToCheck < MaxValue;

    public static ValueAssessment Assess(float valueToCheck, ValueRange[] ranges)
    {
        foreach (ValueRange vr in ranges)
            if (vr.Contains(valueToCheck))
                return vr.Assessment;

        return ValueAssessment.NotDefined;
    }

}