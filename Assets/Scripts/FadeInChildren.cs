﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class FadeInChildren : MonoBehaviour
{

    [SerializeField] private Image[] images;
    Color[] desiredImageColors;
    float[] desiredImageAlphas;

    [SerializeField] private TMP_Text[] texts;
    Color[] desiredTextColors;
    float[] desiredTextAlphas;

    [SerializeField] private float fadeTime;
    private float awakeTime;
    private bool hasFadedIn;

    private void Awake()
    {
        desiredImageColors = new Color[images.Length];
        desiredTextColors = new Color[texts.Length];

        desiredImageAlphas = new float[images.Length];
        desiredTextAlphas = new float[texts.Length];

        for (int i = 0; i < images.Length; i++)
            desiredImageAlphas[i] = images[i].color.a;

        for (int i = 0; i < texts.Length; i++)
            desiredTextAlphas[i] = texts[i].color.a;

        foreach (var image in images)
            image.CrossFadeAlpha(0, 0, false);

        foreach (var text in texts)
            text.CrossFadeAlpha(0, 0, false);
    }

    private void OnEnable()
    {
        for (int i = 0; i < images.Length; i++)
            images[i].CrossFadeAlpha(desiredImageAlphas[i], fadeTime, false);

        for (int i = 0; i < texts.Length; i++)
            texts[i].CrossFadeAlpha(desiredTextAlphas[i], fadeTime, false);
    }

    private void OnDisable()
    {
        for (int i = 0; i < images.Length; i++)
            images[i].CrossFadeAlpha(0, fadeTime, false);

        for (int i = 0; i < texts.Length; i++)
            texts[i].CrossFadeAlpha(0, fadeTime, false);
    }

}
