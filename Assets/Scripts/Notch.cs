﻿using UnityEngine;
using System.Collections;
using TMPro;
using UnityEngine.Assertions;

public class Notch : MonoBehaviour
{

    public bool IsExtremity { get; set; }
    public bool HasBeenConfigured { get; private set; } = false;
    public string NotchValue { get; set; }

    /// <summary>
    /// Value is the actual rotation of the object between (sent by Gauge, currently between 0 and -270 degreess).
    /// </summary>
    private float notchRotation;

    private float startDelay = 0.3f;

    [SerializeField] private TMP_Text valueIndicator;

    private void Awake()
    {
        Assert.IsNotNull(valueIndicator);
        //StartCoroutine(AnimateNotch());
        valueIndicator.text = "";
    }

    public void Configure(bool isExtremity, float notchRotation, string notchValue)
    {
        this.gameObject.SetActive(true);
        IsExtremity = isExtremity;
        this.notchRotation = notchRotation;
        this.gameObject.transform.localEulerAngles = new Vector3(0, 0, notchRotation);
        NotchValue = notchValue;
        valueIndicator.text = NotchValue;
        HasBeenConfigured = true;
    }

    private IEnumerator AnimateNotch()
    {
        while (!HasBeenConfigured)
            yield return null;

        yield return new WaitForSeconds(startDelay);

        while (notchRotation - this.gameObject.transform.rotation.z > 0.5f)
        {
            this.gameObject.transform.Rotate(Vector3.forward, Mathf.Lerp(gameObject.transform.rotation.z, notchRotation, 1f));
            yield return null;
        }

        valueIndicator.text = NotchValue;
    }

}
