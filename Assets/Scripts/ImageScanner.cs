using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.XR.ARFoundation;

public class ImageScanner : MonoBehaviour
{

    [SerializeField] private ARTrackedImageManager trackedImageManager;
    [SerializeField] private GameObject gaugePrefab;
    [SerializeField] private GameObject imageFrame;
    [SerializeField] private Camera aRCamera;

    private ARTrackedImage image;
    private Transform foundImageTransform;

    private const int MAX_ACTIVE_GAUGES = 8;
    private Gauge[] activeGauges = new Gauge[MAX_ACTIVE_GAUGES];
    //private HashSet<ARTrackedImage> allImages = new HashSet<ARTrackedImage>();
    private List<ARTrackedImage> allImages = new List<ARTrackedImage>();
    private Dictionary<string, List<Gauge>> gaugeDictionary = new Dictionary<string, List<Gauge>>();
    private Dictionary<string, Transform> imagePositionDictionary = new Dictionary<string, Transform>();

    private Vector3 gaugeOffset;
    private Vector3 gaugeScale = new Vector3(0.04f, 0.04f, 0.04f);

    private Vector3 sonyOffset1;
    private Vector3 sonyOffset2;

    private Vector3 telenetOffset1;
    private Vector3 telenetOffset2;

    private const float imageFrameMargin = 0.01f;

    // Start is called before the first frame update
    void Start()
    {
        Assert.IsNotNull(gaugePrefab);
        Debug.Log("JB: ImageScanner OK.");

        trackedImageManager.trackedImagesChanged += ShowImageInformation;
        gaugeOffset = new Vector3(0f, 0.1f, 0.03f);

        // x = x, but Z is the altitude, thanks to great modelling skills.
        sonyOffset1 = new Vector3(0.07f, 0, 0.02f);
        sonyOffset2 = new Vector3(0.07f, 0, -0.03f);

        telenetOffset1 = new Vector3(-0.0f, 0f, 0.05f);
        telenetOffset2 = new Vector3(-0.0f, 0f, 0.1f);
    }

    private void ShowImageInformation(ARTrackedImagesChangedEventArgs eventArgs)
    {

        // So, the first time an image is recognized it shows up in "added". Later, if that image appears in-shot, it shows up in "updated".
        // If it's not in "updated", it means that the image was either not in shot (likely) or not recognized (less likely).
        if (eventArgs.added.Count > 0)
        {
            foreach (var image in eventArgs.added)
            {
                var frame = Instantiate(imageFrame, image.transform);
                float imageSize = image.referenceImage.width + imageFrameMargin;
                frame.transform.localScale = new Vector3(imageSize, imageSize, imageSize);

                if (!imagePositionDictionary.ContainsKey(image.referenceImage.name))
                    imagePositionDictionary.Add(image.referenceImage.name, image.transform);

                Gauge gauge = Instantiate(gaugePrefab, image.transform).GetComponent<Gauge>();
                gauge.DesiredPosition = sonyOffset1;
                if (image.referenceImage.name.Contains("telenet"))
                    gauge.DesiredPosition = telenetOffset1;
                gauge.gameObject.transform.localScale = gaugeScale;

                gauge.Initialize(0.1f, 0.6f, "m", "Afstand",
                    new ValueAssessment[] { ValueAssessment.Low, ValueAssessment.Normal, ValueAssessment.High },
                    new float[] { 0.1f, 0.3f, 0.4f, 0.6f });

                if (!gaugeDictionary.ContainsKey(image.referenceImage.name))
                    gaugeDictionary.Add(image.referenceImage.name, new List<Gauge>());
                var gaugeList = gaugeDictionary[image.referenceImage.name];

                gaugeDictionary[image.referenceImage.name].Add(gauge);

                gauge = Instantiate(gaugePrefab, image.transform).GetComponent<Gauge>();
                gauge.DesiredPosition = sonyOffset2;
                if (image.referenceImage.name.Contains("telenet"))
                    gauge.DesiredPosition = telenetOffset2;
                gauge.gameObject.transform.localScale = gaugeScale;

                gauge.Initialize(0, 100, "%", "Batterij",
                    new ValueAssessment[] { ValueAssessment.Danger, ValueAssessment.Normal },
                    new float[] { 0f, 20f, 100f });

                gaugeDictionary[image.referenceImage.name].Add(gauge);

                gaugeList[0].Show = () =>
                {
                    float distance = Vector3.Distance(aRCamera.transform.position, image.transform.position);
                    gaugeList[0].GaugeValue = distance;
                };

                gaugeList[1].Show = () => gaugeList[1].GaugeValue = SystemInfo.batteryLevel * 100;

            }
        }
                
        if (eventArgs.updated.Count > 0)
        {
            foreach (var image in eventArgs.updated)
            {
                imagePositionDictionary[image.referenceImage.name] = image.transform;
                string name = image.referenceImage.name.Split('_')[0];
                HeadsUpDisplay.Instance.Show($"<b>{name}</b> stoommachine");
            }
        }

        if (eventArgs.removed.Count > 0)
        {
            foreach (var image in eventArgs.removed)
            {
                if (imagePositionDictionary.ContainsKey(image.referenceImage.name))
                    imagePositionDictionary.Remove(image.referenceImage.name);
                Debug.Log($"JB: Image {image.referenceImage.name} was removed.");
            }
        }
    }

    public void SetGaugePosition()
    {
        Debug.Log("JB: Trying to (re)locate marker and setting gauge position.");

        foreach (var kvp in gaugeDictionary)
        {
            Transform transform = imagePositionDictionary[kvp.Key];
            var gaugeList = kvp.Value;
            int numberOfGauges = gaugeList.Count;
            for (int i = 0; i < numberOfGauges; i++)
            {
                gaugeList[i].DesiredPosition = transform.position + gaugeOffset + new Vector3(0.15f * i, 0, 0);
                gaugeList[i].DesiredRotation = transform.rotation.eulerAngles;
            }
        }
    }
}
